import unittest
from feed_creator import GoogleMerchantCenterXMLBuilder
from config.config import Config


class IDTestCase(unittest.TestCase):
    def setUp(self):
        self.google = GoogleMerchantCenterXMLBuilder(template=Config.RTB_IND,
                                                     product_dict_list=None)

    def test_id_generate(self):
        code_1 = self.google._create_id(
            contract_condition_code='24V',
            sku_stock_code='sony-xperia-e4-white',
            offer_code='PESI324V',
            tariff_code='EL030'
        )

        self.assertEqual(str(2152277198), code_1)

if __name__ == "__main__":
    unittest.main()