import os

BASE_DIR = os.path.dirname(os.path.dirname(__file__))


class Config(object):

    DATABASE = os.path.join(BASE_DIR, 'database', 'data.sqlite')
    TABLE_NAME = "devices"

    IND = [os.path.join(BASE_DIR, 'ind', 'cennik.xml')]
    MNP = [os.path.join(BASE_DIR, 'mnp', 'cennik.xml')]
    MIG_PSTPD = [os.path.join(BASE_DIR, 'mig', 'postpaid.xml')]
    MIG_MIX = [os.path.join(BASE_DIR, 'mig', 'mix_1.xml'),
               os.path.join(BASE_DIR, 'mig', 'mix_2.xml')]
    SOHO = [os.path.join(BASE_DIR, 'soho', 'cennik_1.xml'),
            os.path.join(BASE_DIR, 'soho', 'cennik_2.xml')]
    MIX = [os.path.join(BASE_DIR, 'mix', 'cennik_1.xml'),
           os.path.join(BASE_DIR, 'mix', 'cennik_2.xml')]
    LAPTOP = [os.path.join(BASE_DIR, 'tablet', 'cennik.xml')]
    MODEM = [os.path.join(BASE_DIR, 'modem', 'cennik.xml')]

    TEMPLATE_DIR = os.path.join(BASE_DIR, 'templates')
    RTB_TEMPLATE = os.path.join(TEMPLATE_DIR, 'MEDIA_TRACKER.xml')
    GOOGLE_TEMPLATE = os.path.join(TEMPLATE_DIR, 'ZANOX.xml')
    QUARTIC_TEMPLATE = os.path.join(TEMPLATE_DIR, 'QUARTIC.xml')

    FEED_DIR = os.path.join(BASE_DIR, 'product_feeds')

    RTB_IND = os.path.join(FEED_DIR, 'rtb_ind.xml')
    RTB_MIX = os.path.join(FEED_DIR, 'rtb_mix.xml')
    RTB_SOHO = os.path.join(FEED_DIR, 'rtb_soho.xml')
    RTB_IND_MIG = os.path.join(FEED_DIR, 'rtb_ind_cnv.xml')
    RTB_MIG_MIX = os.path.join(FEED_DIR, 'rtb_mix_cnv.xml')


    RTB_WHOLE = os.path.join(FEED_DIR, 'rtb_all.xml')

    GOOGLE_FEED = os.path.join(FEED_DIR, 'googlemerchantcenter.xml')
    QUARTIC_FEED = os.path.join(FEED_DIR, 'quartic.xml')