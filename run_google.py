# -*- coding: utf-8 -*-
from config.config import Config
from feed_creator import DataBuilder, GoogleMerchantCenterXMLBuilder, database
from database.storage import tariffs_prices


if __name__ == "__main__":

    template = Config.GOOGLE_TEMPLATE
    db = database.DatabaseHandler(Config.DATABASE, Config.TABLE_NAME)
    photo_dict = db.create_photo_dict()
    print "Mam photo dict"
    print photo_dict

    builder = DataBuilder(
        process="IND",
        file_path=Config.IND,
        price_mapper=tariffs_prices,
        photo_dict=photo_dict
    )

    builder.create_product_dict_list()
    print u"mam listę słowników"
    print len(builder.product_dict_list)

    builder.the_cheapest_products()
    print u"mam najtańsze"
    print len(builder.product_dict_list)

    # builder.mark_availability()
    # print u"Zaznaczyłem dostępność"
    # builder.remove_unavailable()

    xml_handler = GoogleMerchantCenterXMLBuilder(
        template=template,
        product_dict_list=builder.product_dict_list,
        google=True
    )
    ind_entries = xml_handler.create_all_elements()

    builder = DataBuilder(
        process="DATA",
        file_path=Config.MODEM,
        price_mapper=tariffs_prices,
        photo_dict=photo_dict
    )
    builder.create_product_dict_list()
    builder.the_cheapest_products()
    # builder.mark_availability()
    # builder.remove_unavailable()

    xml_handler = GoogleMerchantCenterXMLBuilder(
        template=template,
        product_dict_list=builder.product_dict_list,
        google=True
    )
    modem_entries = xml_handler.create_all_elements()

    builder = DataBuilder(
        process="DATA",
        file_path=Config.LAPTOP,
        price_mapper=tariffs_prices,
        photo_dict=photo_dict,
        laptop=True
    )
    builder.create_product_dict_list()
    builder.the_cheapest_products()
    # builder.mark_availability()
    # builder.remove_unavailable()

    xml_handler = GoogleMerchantCenterXMLBuilder(
        template=template,
        product_dict_list=builder.product_dict_list,
        google=True
    )
    tab_entries = xml_handler.create_all_elements()

    builder = DataBuilder(
        process="MIX",
        file_path=Config.MIX,
        price_mapper=tariffs_prices,
        photo_dict=photo_dict
    )
    builder.create_product_dict_list()
    builder.the_cheapest_products()
    # builder.mark_availability()
    # builder.remove_unavailable()

    xml_handler = GoogleMerchantCenterXMLBuilder(
        template=template,
        product_dict_list=builder.product_dict_list,
        google=True
    )
    mix_entries = xml_handler.create_all_elements()

    builder = DataBuilder(
        process="MIG_PSTPD",
        file_path=Config.MIG_PSTPD,
        price_mapper=tariffs_prices,
        photo_dict=photo_dict
    )

    builder.create_product_dict_list()
    builder.the_cheapest_products()
    # builder.mark_availability()
    # builder.remove_unavailable()

    xml_handler = GoogleMerchantCenterXMLBuilder(
        template=template,
        product_dict_list=builder.product_dict_list,
        google=True
    )
    mig_pstpd_entries = xml_handler.create_all_elements()

    builder = DataBuilder(
        process="MIG_MIX",
        file_path=Config.MIG_MIX,
        price_mapper=tariffs_prices,
        photo_dict=photo_dict
    )

    builder.create_product_dict_list()
    builder.the_cheapest_products()
    # builder.mark_availability()
    # builder.remove_unavailable()

    xml_handler = GoogleMerchantCenterXMLBuilder(
        template=template,
        product_dict_list=builder.product_dict_list,
        google=True
    )

    mig_mix_entries = xml_handler.create_all_elements()

    entries = ind_entries + modem_entries + tab_entries + mix_entries + mig_pstpd_entries + mig_mix_entries


    xml_handler.build_xml_file(entries)

    xml_handler.save_feed(output_file_path=Config.GOOGLE_FEED)

    print "gotowe"