# -*- coding: utf-8 -*-
from datetime import date
from binascii import crc32
from xml.dom import minidom
from lxml import etree
from lxml.etree import CDATA
import requests
from bs4 import BeautifulSoup as Soup


class DataBuilder(object):

    def __init__(self, process, file_path, price_mapper, laptop=False,
                 photo_dict=None):
        """
        :param process: IND, MIX, DATA lub SOHO
        :param file_path: Ścieżka do katalogu z cennikami
        :param price_mapper: plik z mapowaniem planów cenowych na wysokość abo
        :param laptop: boolean określający czy oferta jest z laptopem lub tab
        :param photo_dict: słownik zdjęć dostarczony instacji klasy
        """
        self.process = process
        self.file_path = file_path
        self.price_mapper = price_mapper
        self.product_dict_list = []
        self.product_url = ""
        self.market_type = ""
        self.process_segment = ""
        self.device_type_code = ""
        self.laptop = laptop
        self.photo_dict = photo_dict

        self.available_dict = {}

        self.type_to_name_dict = {
            "PHONE": u"telefon",
            "TAB": u"tablet",
            "MODEM": u"modem",
            "IND.NEW.POSTPAID.ACQ": u"w abonamencie",
            "IND.SUB.MIG.POSTPAID": u"w abonamencie",
            "SOHO.NEW.POSTPAID.ACQ": u"w abonamencie",
            "IND.NEW.MIX.ACQ": u"w mixie",
            "IND.SUB.MIG.MIX": u"w mixie"
        }

    def set_defaults_per_process(self):
        """
        Domyślne wartości w zależności do procesu. Metodę należy wywołać przed
        zbudowaniem listy słowników produktów
        """
        if self.process == "MIX":
            self.product_url = "http://plus.pl/telefon?"
            self.market_type = "IND"
            self.process_segment = "IND.NEW.MIX.ACQ"
            self.device_type_code = "PHONE"

        elif self.process == "DATA":
            self.product_url = "http://plus.pl/tablet-laptop?" if self.laptop \
                else "http://plus.pl/modem-router?"
            self.market_type = "IND"
            self.process_segment = "IND.NEW.POSTPAID.ACQ"
            self.device_type_code = "TAB" if self.laptop else "MODEM"

        elif self.process == "SOHO":
            self.product_url = "https://www.plus.pl/dla-firm/telefon?"
            self.market_type = "SOHO"
            self.process_segment = "SOHO.NEW.POSTPAID.ACQ"
            self.device_type_code = "PHONE"

        elif self.process == "MIG_PSTPD":
            self.product_url = "http://plus.pl/telefon?"
            self.market_type = "IND"
            self.process_segment = "IND.SUB.MIG.POSTPAID"
            self.device_type_code = "PHONE"

        elif self.process == "MIG_MIX":
            self.product_url = "http://plus.pl/telefon?"
            self.market_type = "IND"
            self.process_segment = "IND.SUB.MIG.MIX"
            self.device_type_code = "PHONE"

        elif self.process == "MNP":
            self.product_url = "http://plus.pl/telefon?"
            self.market_type = "IND"
            self.process_segment = "IND.NEW.POSTPAID.MNP"
            self.device_type_code = "PHONE"

        else:
            self.product_url = "http://plus.pl/telefon?"
            self.market_type = "IND"
            self.process_segment = "IND.NEW.POSTPAID.ACQ"
            self.device_type_code = "PHONE"

    @staticmethod
    def _slugify(name):
        """
        :param name: Kod modelu urządzenia pobierany z pliku xml cennika
        :return: sku-stock-code na potrzeby adresu url karty produktu
        """
        name = name.lower()
        name = name.replace('+', '')
        name = name.replace('-', '')
        name = name.replace(u'ó', 'o')
        name = name.replace(u'ą', 'a')
        name = name.replace(u'ę', 'e')
        name = name.replace(u'ć', 'c')
        name = name.replace(u'ń', 'n')
        name = name.replace(u'ł', 'l')
        name = name.replace(u'ś', 's')
        name = name.replace(u'ź', 'z')
        name = name.replace(u'ż', 'z')
        name = name.replace(' ', '-')
        return name

    def _map_price(self, tariff_plan_code):
        return self.price_mapper.get(tariff_plan_code)

    @staticmethod
    def _create_url(url_begin, **kwargs):
        """
        jako argumennty słów kluczowych należy wywołać parametry urla
        """
        url_end = "&".join(["%s=%s" % (k, v) for k, v in kwargs.items()])
        return url_begin + url_end

    @staticmethod
    def _rtb_url(url):
        url = url.replace(':', r'%3A')
        url = url.replace('/', r'%2F')
        url = url.replace('?', r'%3F')
        url = url.replace('=', r'%3D')
        url = url.replace('&', r'%26')
        return url

    @staticmethod
    def _check_availability(url):
        r = requests.get(url)
        html = r.content
        parsed_html = Soup(html)
        div = parsed_html.find('div', id="order-device")
        link = div.find('a')
        if link and 'Do koszyka' in link.text:
            return True
        return False

    def _check_availability_from_json(self, sku):
        r = requests.post(
            url='http://www.plus.pl/telefon?p_p_id=ajaxportlet_WAR_frontend_INSTANCE_T3lq&p_p_lifecycle=2&p_p_resource_id=deviceAvailable',
            data={'deviceStockCode': sku}
        )
        response = r.json()
        availability = response['deviceAvailables'][0]['available']
        self.available_dict[sku] = availability

    def _get_photo(self, sku):
        return self.photo_dict.get(sku)

    def _product_dict(self, price_list_item, offer_code):
        d = dict()
        d['tariff_plan'] = price_list_item[0].text
        d['abo_price'] = self._map_price(d['tariff_plan'])
        d['offer'] = offer_code
        d['name'] = price_list_item[1].text
        d['manufacturer'] = price_list_item[1].text.split()[0]
        d['sku'] = self._slugify(price_list_item[1].text)
        d['price'] = float(price_list_item[2][1].text)
        d['contract_condition'] = price_list_item[3].text
        d['photo'] = self._get_photo(d['sku'])
        d['type'] = self.device_type_code
        d['segment'] = self.process_segment
        if d['sku'] not in self.available_dict:
            self._check_availability_from_json(d['sku'])
        d['available'] = True if self.available_dict[d['sku']] == 'AVAILABLE' \
            else False
        url = self._create_url(
            self.product_url,
            deviceTypeCode=self.device_type_code,
            deviceStockCode=d['sku'],
            processSegmentationCode=self.process_segment,
            marketTypeCode=self.market_type,
            offerNSICode=offer_code,
            contractConditionCode=d['contract_condition'],
            tariffPlanCode=d['tariff_plan'],
        )
        url += "&offerSegmentationCodes=&skipOfferSegmentationCodes=WYPRZ,WYPRZ_MNP,IND.NEW.MIX.ACQ.PLUSH.EL,IND.NEW.MIX.ACQ.PLUSH.SIM,IND.NEW.MIX.ACQ.PLUSH.ST"
        d['nsi_url'] = url
        d['rtb_url'] = 'https://www.plus.pl/nsi_tradetracker/' + \
            '?tt=16815_642127_164759_&amp;r=' + self._rtb_url(url)
        modified_url = self._create_url(
            self.product_url,
            deviceTypeCode=self.device_type_code,
            deviceStockCode=d['sku'],
            processSegmentationCode="RTB_processSegmentationCode",
            marketTypeCode="RTB_marketTypeCode",
            offerNSICode="RTB_offerNSICode",
            contractConditionCode="RTB_contractConditionCode",
            tariffPlanCode="RTB_tariffPlanCode",
            offerSegmentationCodes="RTB_offerSegmentationCodes",
            skipOfferSegmentationCodes="RTB_skipOfferSegmentationCodes"
        )
        d['modified_url'] = 'https://www.plus.pl/nsi_tradetracker/' + \
            '?tt=16815_642127_164759_&amp;r=' + self._rtb_url(modified_url)

        begin = self.type_to_name_dict.get(d['type'], "telefon")
        middle = self.type_to_name_dict.get(d['segment'],
                                            "w abonamencie")
        phone_price = int(d['price'])
        end = u"za %d zł" % phone_price

        d['short_description'] = begin + " " + middle + " " + end

        return d

    def create_product_dict_list(self):
        self.set_defaults_per_process()
        for _file in self.file_path:
            tree = etree.parse(_file)
            print "cennik sparsowany"
            root = tree.getroot()
            price_lists = root.findall('PriceList')
            for price_list in price_lists:
                offer_code = price_list[0].text
                price_list_items = price_list.findall('PriceListItem')
                for price_list_item in price_list_items:
                    d = self._product_dict(price_list_item, offer_code)
                    if d.get('photo'):
                        self.product_dict_list.append(d)

    def the_cheapest_products(self):
        to_remove = []
        for product in self.product_dict_list:
            iterator = [
                elem for elem in self.product_dict_list
                if elem['name'] == product['name']
            ]
            the_cheapest = iterator[0]
            for product_dict in iterator[1:]:
                if product_dict['price'] > the_cheapest['price']:
                    if product_dict not in to_remove:
                        to_remove.append(product_dict)
                elif product_dict['price'] == the_cheapest['price']:
                    if product_dict['abo_price'] < the_cheapest['abo_price']:
                        if the_cheapest not in to_remove:
                            to_remove.append(the_cheapest)
                        the_cheapest = product_dict
                    else:
                        if product_dict not in to_remove:
                            to_remove.append(product_dict)
                else:
                    if the_cheapest not in to_remove:
                        to_remove.append(the_cheapest)
                    the_cheapest = product_dict

        for item in to_remove:
            self.product_dict_list.remove(item)

    @staticmethod
    def _get_description(url):
        r = requests.get(url)
        html = r.content
        parsed_html = Soup(html)
        desc = parsed_html.find('div', id="deviceinfo")
        return desc.text.strip()

    def mark_availability(self):
        for product_dict in self.product_dict_list:
            url = product_dict['nsi_url']
            print u"Sprawdzam dostępność %s" % product_dict['name']
            print u"Łącze się z %s" % url
            product_dict['description'] = self._get_description(url)
            if self._check_availability(url):
                product_dict['available'] = True
            else:
                product_dict['available'] = False

    def remove_unavailable(self):
        for_remove = []
        for product_dict in self.product_dict_list:
            if not product_dict['available']:
                for_remove.append(product_dict)

        for item in for_remove:
            self.product_dict_list.remove(item)


class RTBXMLBuilder(object):

    def __init__(self, template, product_dict_list):
        self.template = template
        self.product_dict_list = product_dict_list
        self.root = etree.parse(self.template).getroot()

    def save_feed(self, output_file_path):
        """
        Stworzenie pliku z feedem w zadanej lokalizacji. Pretty print
        :param output_file_path: ściezka do zapisu pliki
        """
        xml_text = etree.tostring(self.root)
        with open(output_file_path, 'wb') as xml:
            xml.write(xml_text)
        reparsed = minidom.parse(output_file_path)
        pretty = reparsed.toprettyxml(encoding="utf-8")
        with open(output_file_path, 'wb') as xml:
            xml.write(pretty)

    def _set_date(self):
        self.root.attrib['timestamp'] = date.today().isoformat()

    @staticmethod
    def _create_sub_elem(parent, name, value=None,
                         attrib=None, attrib_value=None):
        sub_elem = etree.SubElement(parent, name)
        if value:
            sub_elem.text = value
        if attrib and attrib_value:
            sub_elem.attrib[attrib] = attrib_value
        return sub_elem

    @staticmethod
    def extra(parent, attrib, value):
        """
        Metoda pomocnicza by zapewnić DRY
        """
        field = etree.SubElement(parent, 'field')
        field.attrib['name'] = attrib
        field.attrib['value'] = value
        return field

    @staticmethod
    def _make_category(product_dict):
        segment = product_dict['segment'].replace('.', '-')
        device_type = 'PHONE' if product_dict['type'] == 'PHONE' else 'DATA'
        return "-%s-%s" % (segment, device_type)

    def _create_elem(self, product_dict):
        product = etree.Element('product')
        product.attrib["id"] = product_dict['sku'] \
            + self._make_category(product_dict)
        self._create_sub_elem(product, 'name', value=product_dict['name'])
        self._create_sub_elem(product, 'price',
                              value=str(product_dict['price']),
                              attrib='currency', attrib_value='PLN')
        self._create_sub_elem(product, 'NSI_URL', value=product_dict['nsi_url'])
        self._create_sub_elem(product, 'URL', value=product_dict['rtb_url'])
        images = self._create_sub_elem(product, 'images')
        self._create_sub_elem(images, 'image',
                              value="http://plus.pl" + product_dict['photo'])
        desc = self._create_sub_elem(product, 'description')
        cdata = CDATA(' ')
        desc.text = cdata
        self._create_sub_elem(product, 'categories')
        properties = self._create_sub_elem(product, 'properties')
        prop = self._create_sub_elem(properties, 'property', attrib='name',
                                     attrib_value='manufacturer')
        self._create_sub_elem(prop, 'value', value=product_dict['manufacturer'])
        prop = self._create_sub_elem(properties, 'property', attrib='name',
                                     attrib_value='category')
        self._create_sub_elem(prop, 'value',
                              value=self._make_category(product_dict)[1:])
        prop = self._create_sub_elem(properties, 'property', attrib='name',
                                     attrib_value='modified_url')
        self._create_sub_elem(prop, 'value', value=product_dict['modified_url'])
        prop = self._create_sub_elem(properties, 'property', attrib='name',
                                     attrib_value='country')
        self._create_sub_elem(prop, 'value', value='Poland')
        prop = self._create_sub_elem(properties, 'property', attrib='name',
                                     attrib_value='deliveryCosts')
        self._create_sub_elem(prop, 'value', value='0.00')
        prop = self._create_sub_elem(properties, 'property', attrib='name',
                                     attrib_value='abo_price')
        self._create_sub_elem(prop, 'value',
                              value=str(product_dict['abo_price']))
        prop = self._create_sub_elem(properties, 'property', attrib='name',
                                     attrib_value='available')
        self._create_sub_elem(prop, 'value',
                              value=str(product_dict['available']))
        prop = self._create_sub_elem(properties, 'property', attrib='name',
                                     attrib_value='available_for_order')
        for_order = "true" if product_dict['available'] else "false"
        self._create_sub_elem(prop, 'value', value=for_order)
        prop = self._create_sub_elem(properties, 'property', attrib='name',
                                     attrib_value='processSegmentationCode')
        self._create_sub_elem(prop, 'value', value=product_dict['segment'])

        return product

    def create_all_elements(self):
        entries = []
        self._set_date()
        for product_dict in self.product_dict_list:
            entry = self._create_elem(product_dict)
            entries.append(entry)
        return entries

    def build_xml_file(self, entries):
        for entry in entries:
            self.root.append(entry)


class GoogleMerchantCenterXMLBuilder(RTBXMLBuilder):

    def __init__(self, available_only=True, google=True, **kwargs):
        super(GoogleMerchantCenterXMLBuilder, self).__init__(**kwargs)
        self.available_only = available_only
        self.google = google
        self.color_dict = dict()
        self.categories = {
            'IND-NEW-POSTPAID-ACQ-PHONE': {'id': '1'},
            'IND-NEW-POSTPAID-ACQ-DATA': {'id': '2'},
            'IND-NEW-POSTPAID-MNP-PHONE': {'id': '3'},
            'IND-NEW-MIX-ACQ-PHONE': {'id': '4'},
            'IND-SUB-MIG-MIX-PHONE': {'id': '5'},
            'IND-SUB-MIG-POSTPAID-PHONE': {'id': '6'},
            'IND-SUB-RET-POSTPAID-PHONE': {'id': '7'},
            'IND-SUB-RET-POSTPAID-DATA': {'id': '8'},
            'IND-SUB-RET-MIX-PHONE': {'id': '9'},
            'IND-SUB-SAT-POSTPAID-PHONE': {'id': '10'},
            'IND-SUB-SAT-POSTPAID-DATA': {'id': '11'},
            'SOHO-NEW-POSTPAID-ACQ-PHONE': {'id': '12'},
            'SOHO-NEW-POSTPAID-ACQ-DATA': {'id': '13'},
        }

    @staticmethod
    def _create_id(offer_code, tariff_code,
                   sku_stock_code, contract_condition_code):
        crc = crc32("%s_%s_%s_%s" % (sku_stock_code,
                                     offer_code,
                                     tariff_code,
                                     contract_condition_code)) & 0xffffffff
        crc = str(crc)
        return crc[:-1] if crc.endswith('L') else crc

    def _set_date(self, title='XML FEED'):
        title_elem = self.root.find(
            '{http://www.w3.org/2005/Atom}title')
        title_elem.text = title
        date_elem = self.root.find(
            '{http://www.w3.org/2005/Atom}updated')
        date_elem.text = "%sT00:00:00Z" % date.today().isoformat()

    def _sub_elem(self, parent, elem_name):
        if self.google:
            return etree.SubElement(
                parent, "{http://base.google.com/ns/1.0}%s" % elem_name
            )
        else:
            return etree.SubElement(parent, elem_name)

    def _create_elem(self, product_dict):
        entry = etree.Element('entry')
        title = self._sub_elem(entry, 'title')
        title.text = product_dict['name']
        id_elem = self._sub_elem(entry, 'id')
        if self.google:
            id_elem.text = self._create_id(
                product_dict['offer'],
                product_dict['tariff_plan'],
                product_dict['sku'],
                product_dict['contract_condition']
            )
        else:
            id_elem.text = product_dict['sku']
            manufacturer = self._sub_elem(entry, 'manufacturer')
            manufacturer.text = product_dict['manufacturer']
        link = self._sub_elem(entry, 'link')
        link.text = product_dict['nsi_url']
        if not self.google:
            abo_price = self._sub_elem(entry, 'abo_price')
            abo_price.text = str(product_dict['abo_price'])
        description = self._sub_elem(entry, 'description')
        description.text = product_dict['short_description']
        availability = self._sub_elem(entry, 'availability')
        availability.text = 'in stock' if product_dict['available'] \
            else 'out of stock'
        image_link = self._sub_elem(entry, 'image_link')
        image_link.text = 'http://plus.pl' + product_dict['photo']
        price = self._sub_elem(entry, 'price')
        price.text = str(product_dict['price'])
        condition = self._sub_elem(entry, 'condition')
        condition.text = 'new'
        product_category = self._sub_elem(entry, 'google_product_category')
        if product_dict['type'] == 'TAB':
            product_category.text = "Elektronika > Komputery > Tablety"
        elif product_dict['type'] == 'MODEM':
            product_category.text = "Elektronika > Sieci > Modemy"
        else:
            text = u"Elektronika > Komunikacja > Telefonia > Telefony komórkowe > Smartfony"
            product_category.text = text
        product_type = self._sub_elem(entry, 'product_type')
        product_type.text = product_category.text
        shipping = self._sub_elem(entry, 'shipping')
        country = self._sub_elem(shipping, 'country')
        country.text = "PL"
        service = self._sub_elem(shipping, 'service')
        service.text = 'Standard Rate'
        shipping_price = self._sub_elem(shipping, 'price')
        shipping_price.text = '0.00'
        custom_label = self._sub_elem(entry, 'custom_label_1')
        if 'SOHO' in product_dict['segment']:
            custom_label.text = "SOHO"
        elif 'MIX' in product_dict['segment']:
            custom_label.text = 'MIX'
        elif product_dict['type'] == 'MODEM' or product_dict['type'] == 'TAB':
            custom_label.text = 'INTERNET'
        else:
            custom_label.text = 'PLUS ABO'
        custom_label_2 = self._sub_elem(entry, 'custom_label_2')
        custom_label_2.text = product_dict['manufacturer']
        # custom_label_3 = self._sub_elem(entry, 'custom_label_3')
        # custom_label_3.text = CDATA(product_dict['nsi_url'])
        return entry


class QuarticXMLBuilder(GoogleMerchantCenterXMLBuilder):
    def __init__(self, **kwargs):
        super(QuarticXMLBuilder, self).__init__(**kwargs)

    def _set_date(self, title='XML FEED'):
        pass

    def _create_elem(self, product_dict):
        product = etree.Element('product')
        self._create_sub_elem(
            parent=product,
            name='id',
            value=self._create_id(
                product_dict['offer'],
                product_dict['tariff_plan'],
                product_dict['sku'],
                product_dict['contract_condition']
            )
        )
        self._create_sub_elem(parent=product, name='title',
                              value=CDATA(product_dict['name']))
        self._create_sub_elem(parent=product, name='price',
                              value=str(product_dict['price']))
        self._create_sub_elem(parent=product, name='link',
                              value=CDATA(product_dict['nsi_url']))
        self._create_sub_elem(parent=product, name='thumb',
                              value='http://plus.pl' + product_dict['photo'])
        self._create_sub_elem(parent=product, name='status',
                              value='1' if product_dict['available'] else '0')
        self._create_sub_elem(parent=product, name='category_1',
                              attrib='id',
                              attrib_value=self.categories[self._make_category(product_dict)[1:]]['id'],
                              value=self._make_category(product_dict)[1:])

        return product

    def save_feed(self, output_file_path):
        """
        Stworzenie pliku z feedem w zadanej lokalizacji. Pretty print
        :param output_file_path: ściezka do zapisu pliki
        """
        with open(output_file_path, "w") as xml:
            xml.write(etree.tostring(self.root, pretty_print=True))
