# -*- coding: utf-8 -*-
import sqlite3 as sql


class DatabaseHandler:

    def __init__(self, database_name, table_name):
        self.database_name = database_name
        self.table_name = table_name

    def create_table(self):
        with sql.connect(self.database_name) as connection:
            cursor = connection.cursor()
            try:
                cursor.execute("""CREATE TABLE %s
                    (id INTEGER PRIMARY KEY, sku TEXT, photo_url TEXT)"""
                               % self.table_name)
            except sql.OperationalError:
                return "Tabela o podanej nazwie już istnieje"

    def drop_table(self):
        with sql.connect(self.database_name) as connection:
            cursor = connection.cursor()
            cursor.execute("""DROP TABLE if exists %s""" % self.table_name)

    def insert_one_device(self, sku_stock_code, photo_url, update=False):
        with sql.connect(self.database_name) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT photo_url from %s WHERE sku=?"
                           % self.table_name, (sku_stock_code,))
            result = cursor.fetchone()
            if not result:
                cursor.execute("""INSERT INTO %s(sku, photo_url)
                    VALUES(?, ?)""" % self.table_name, (sku_stock_code,
                                                        photo_url))
            elif update:
                cursor.execute("""UPDATE %s SET photo_url=? WHERE sku=?"""
                               % self.table_name, (photo_url, sku_stock_code))

    def insert_all_devices(self, dev_list):
        """Tylko do pierwszego wsadu do bazy"""
        with sql.connect(self.database_name) as connection:
            cursor = connection.cursor()
            cursor.executemany("""INSERT INTO %s(sku, photo_url)
                VALUES (?, ?)""" % self.table_name, dev_list)

    def _fetch_devices(self):
        """Prosty SELECT"""
        with sql.connect(self.database_name) as connection:
            cursor = connection.cursor()
            cursor.execute("SELECT sku, photo_url FROM %s" % self.table_name)
            return cursor.fetchall()

    def create_photo_dict(self):
        """
        Wyciąga z bazy danych tylko te produkty, które mają zdjecia i zwraca je
        w formie słownika
        """
        with sql.connect(self.database_name) as db:
            cursor = db.cursor()
            cursor.execute("""SELECT sku, photo_url FROM %s WHERE
                photo_url != 'bad_url'""" % self.table_name)
            photos = cursor.fetchall()
            return dict(photos)