# -*- coding: utf-8 -*-
from config.config import Config
from feed_creator import DataBuilder, RTBXMLBuilder, database
from database.storage import tariffs_prices


if __name__ == "__main__":

    template = Config.RTB_TEMPLATE
    db = database.DatabaseHandler(Config.DATABASE, Config.TABLE_NAME)
    photo_dict = db.create_photo_dict()
    print "Mam photo dict"
    print photo_dict

    builder = DataBuilder(
        process="IND",
        file_path=Config.IND,
        price_mapper=tariffs_prices,
        photo_dict=photo_dict
    )
    builder.create_product_dict_list()
    print u"mam listę słowników"
    print len(builder.product_dict_list)

    builder.the_cheapest_products()
    print u"mam najtańsze"
    print len(builder.product_dict_list)

    # builder.mark_availability()
    # print u"Zaznaczyłem dostępność"

    # builder.remove_unavailable()
    # print u"Usunąłem niedostępne"

    xml_handler = RTBXMLBuilder(template=template,
                                product_dict_list=builder.product_dict_list)
    ind_entries = xml_handler.create_all_elements()

    '''builder = DataBuilder(
        process="DATA",
        file_path=Config.MODEM,
        price_mapper=tariffs_prices,
        photo_dict=photo_dict
    )
    builder.create_product_dict_list()
    builder.the_cheapest_products()
    builder.mark_availability()

    xml_handler = RTBXMLBuilder(template=template,
                                product_dict_list=builder.product_dict_list)
    modem_entries = xml_handler.create_all_elements()

    builder = DataBuilder(
        process="DATA",
        file_path=Config.LAPTOP,
        price_mapper=tariffs_prices,
        photo_dict=photo_dict,
        laptop=True
    )
    builder.create_product_dict_list()
    builder.the_cheapest_products()
    builder.mark_availability()

    xml_handler = RTBXMLBuilder(template=template,
                                product_dict_list=builder.product_dict_list)
    tab_entries = xml_handler.create_all_elements()'''

    entries = ind_entries  # + modem_entries + tab_entries

    xml_handler.build_xml_file(entries)

    xml_handler.save_feed(output_file_path=Config.RTB_IND)

    print "gotowe"